steghide (0.5.1-15) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Fix field name case in debian/copyright (Upstream-name => Upstream-Name).
  * Update standards version to 4.5.0, no changes needed.

  [ Giovani Augusto Ferreira ]
  * Bumped DH level to 13.
  * debian/steghide.docs: updated file paths.
  * debian/not-installed: added to list not installed files.
  * debian/control: moved doxygen to Build-Depends.
  * debian/upstream/metadata: added bug tracking URL.
  * debian/copyright: updated packaging years.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian
  * d/t/control: Add superficial restriction (closes: #969869)

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sun, 27 Sep 2020 13:41:52 -0300

steghide (0.5.1-14) unstable; urgency=medium

  [ Giovani Augusto Ferreira ]
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: Changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added the field Rules-Requires-Root: binary-targets.
      - Standards-Version: bumped to 4.4.1 (no changes needed)
      - Set "Multi-Arch: foreign" for steghide-doc.
  * debian/copyright:
      - Updated years and rights.
  * debian/patches/:
      - add-hardening.patch: created to improve hardening.
      - fix-spelling.patch: updated indexes.
  * debian/salsa-ci.yml:
      - Allow reprotest to fail temporarily.
  * debian/tests/control: created to provide trivial tests.
  * wrap-and-sort -sa

  [ Samuel Henrique ]
  * Add salsa-ci.yml
  * d/steghide-doc.doc-base: Fix typo steghise

 -- Giovani Augusto Ferreira <giovani@debian.org>  Thu, 26 Dec 2019 19:41:22 -0300

steghide (0.5.1-13) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Giovani Augusto Ferreira ]
  * Bumped DH level to 11
  * Bumped Standards-Version to 4.2.1
  * Dropped autoreconf depends
  * Updated my email address
  * debian/copyright: updated URI and packaging copyright years
  * debian/patches: removed obsolete patches.
  * debian/rules: added override to dh_auto_test
  * Added upstream metadata file

 -- Giovani Augusto Ferreira <giovani@debian.org>  Wed, 29 Aug 2018 19:02:22 -0300

steghide (0.5.1-12) unstable; urgency=medium

  * New Maintainer. (Closes: #826880)
  * Acknowledge NMU. Thanks to Matthias Klose. (Closes: #761788)
  * debian/control:
      - Added libjs-jquery for steghide-doc.
      - Added the Vcs-* fields.
      - Fixed a mistake in a long description.
      - Removed the leading article from short descriptions.
      - Using dh-autoreconf now. (Closes: #707769)
  * debian/copyright:
      - Using 1.0 format.
      - Full updated.
  * debian/docs: renamed to steghide.docs.
  * debian/patches:
      - consolidated-gcc-patches.patch:
          ~ Added index files reference.
      - correct-german.patch and
      - no-mkinstalldirs.patch:
          ~ Added a header to patch.
      - fix-spelling.patch:
          ~ Created to fix few typos in some files.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS to improve the GCC hardening.
      - Added command to remove jquery.js from upstream source code.
      - Disabled verbose mode.
  * debian/steghide.install:
      - Removed entry that was installing useless files. (Closes: #560163)
  * debian/steghide-doc.doc-base:
      - Created to link the developer documentation.
  * debian/steghide-doc.links:
      - Added to properly link with libjs-jquery
  * debian/watch: created.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 19 Nov 2016 18:31:07 -0200

steghide (0.5.1-11) unstable; urgency=medium

  * QA upload.
  * debian/compat:
    - Bumped compat level to 10
      this adds with --with-autoreconf to update config.{guess,sub}
      and fix FTBFS on architectures such as ppc64el.
      Closes: #759453, #535842
  * debian/source/format:
    - File added with "3.0 (Quilt)"
  * debian/control:
    - Standards-Version updated to 3.9.8.
    - ${misc:Depends} to binary package dependency.
    - Added entry for the steghide-doc which now will contain the
      documentation generated.
  * debian/rules:
    - Changed to dh $@ to make debian build simpler.
  * debian/patches:
    - Updated to quilt format. Consolidated modifications shipped in
      file diff.gz to 2 separated patches.
    - Some patches to enable build system to run with autoreconf.

 -- Fernando Seiti Furusato <ferseiti@linux.vnet.ibm.com>  Mon, 26 Sep 2016 11:51:42 -0400

steghide (0.5.1-10) unstable; urgency=medium

  * Package orphaned, maintainer is now the QA group.
  * Changed to debhelper compatibility 5. Closes: #817680.

 -- Ola Lundqvist <opal@debian.org>  Thu, 09 Jun 2016 20:00:31 +0200

steghide (0.5.1-9.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build-depend on libtool-bin. Closes: #761788.

 -- Matthias Klose <doko@debian.org>  Thu, 09 Oct 2014 10:09:35 +0000

steghide (0.5.1-9) unstable; urgency=low

  * Applied patch from Cyril Brulebois <cyril.brulebois@enst-
    bretagne.fr> in order to build with gcc-4.3. Closes: #455394.

 -- Ola Lundqvist <opal@debian.org>  Mon, 17 Mar 2008 07:40:19 +0100

steghide (0.5.1-8) unstable; urgency=low

  * Updated to standards version 3.7.2.
  * Maintainer upload, closes: #360479.
  * Updated to debhelper 4 compatibility.

 -- Ola Lundqvist <opal@debian.org>  Wed, 30 Aug 2006 07:54:24 +0200

steghide (0.5.1-7.1) unstable; urgency=high

  * Non-maintainer upload
  * EmbData.cc: Explicitly include config.h to work around libmhash's
    namespace abuse of VERSION
  * Forcefully cast the return of mhash_get_hash_name from uint8_t * to char *
  * Happy birthday, vorlon, closes: #360479

 -- Erinn Clark <erinn@double-helix.org>  Thu, 27 Apr 2006 22:17:11 -0400

steghide (0.5.1-7) unstable; urgency=low

  * Updated config.sub and config.guess to make it possible to build on
    Debian GNU/k*BSD, closes: #332798.
  * Updated standards version to 3.6.2.
    This implies that dh version is not 3 instead of 1 and the
    debian/rules file have been changed accordingly.
  * Updated to new FSF address.

 -- Ola Lundqvist <opal@debian.org>  Sat,  5 Nov 2005 20:26:55 +0100

steghide (0.5.1-6) unstable; urgency=low

  * Now build depend on libjpeg-dev, closes: #324387.
  * Applied patch for German translation, closes: #313898. Thanks to Jens
    Seidel <jensseidel@users.sf.net> for the fix.
  * Applied patch from Matt Kraai <kraai@ftbfs.org> to make it build
    with gcc4, closes: #324056.

 -- Ola Lundqvist <opal@debian.org>  Sat,  8 Oct 2005 18:58:48 +0200

steghide (0.5.1-5) unstable; urgency=low

  * Applied patch from Andreas Jochens <aj@andaco.de> so it can build
    with gcc 3.4, closes: #274184.
  * Updated standards version to 3.6.1.
  * Refixed so that INSTALL and COPYING information is not in the
    deb archive.

 -- Ola Lundqvist <opal@debian.org>  Sat,  2 Oct 2004 20:10:39 +0200

steghide (0.5.1-4) unstable; urgency=low

  * Added libtool to build deps, closes: #239724.

 -- Ola Lundqvist <opal@debian.org>  Sat, 27 Mar 2004 12:07:03 +0100

steghide (0.5.1-3) unstable; urgency=low

  * Added build dependency on zlib, closes: #239277.

 -- Ola Lundqvist <opal@debian.org>  Mon, 22 Mar 2004 08:11:12 +0100

steghide (0.5.1-2) unstable; urgency=low

  * Removed gcc-2.95 from build depends, closes: #19945.

 -- Ola Lundqvist <opal@debian.org>  Sun, 21 Mar 2004 19:05:44 +0100

steghide (0.5.1-1) unstable; urgency=low

  * New upstream release, closes: #227176.
  * This version works with gcc3 with minor patches, closes: #199459.

 -- Ola Lundqvist <opal@debian.org>  Sat, 20 Mar 2004 18:31:36 +0100

steghide (0.4.6b-2) unstable; urgency=low

  * Fixed build dependency so it can compile on sparc, closes: #198138.

 -- Ola Lundqvist <opal@debian.org>  Thu, 17 Jul 2003 08:52:53 +0200

steghide (0.4.6b-1) unstable; urgency=low

  * New maintainer, closes: #174125.
  * New upstream version.
  * Updated to standards version 3.5.8.
  * Reformatted the description so it is a bit more readable.
  * Changed the copyright file so it contains a better description
    of the GPL. Also fixed a lintian error there.
  * Build depends on gcc-2.95 and changed rules file as well.
  * Moved from non-US to misc.

 -- Ola Lundqvist <opal@debian.org>  Wed, 11 Jun 2003 22:39:06 +0200

steghide (0.4.2-0) unstable; urgency=low

  * New upstream version (Closes: #123761)
  * Removed README.Debian, no longer really useful
  * Updated standards version

 -- Brian Russo <wolfie@debian.org>  Wed, 30 Jan 2002 14:40:23 -1000

steghide (0.3.1-4) unstable; urgency=low

  * Adopting package (Closes: #100264)
  * Updated maintainer field in control.. hurrah

 -- Brian Russo <wolfie@debian.org>  Thu, 14 Jun 2001 14:02:54 +1100

steghide (0.3.1-3) unstable; urgency=low

  * Corrected typo in copyright file.
  * Removed installation of INSTALL file to fix lintian warning.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Thu, 29 Mar 2001 15:20:56 +0200

steghide (0.3.1-2) unstable; urgency=low

  * Corrected the Section in control, and the following;
    closes: #81013

 -- Dr. Guenter Bechly <gbechly@debian.org>  Fri,  9 Feb 2001 21:20:07 +0100

steghide (0.3.1-1) unstable; urgency=low

  * Initial Release, closes: Bug #81013.
  * Included the test-image testimage.bmp.gz from the steghide website.
  * Commented out the installation of docs in Makefile.am.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Mon,  1 Jan 2001 20:24:27 +0100
